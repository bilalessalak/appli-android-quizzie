package com.example.game.model;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

public class QuestionBank implements Serializable { //cette classe s'occupe de la gestion de la liste des questions

    private List<Question> mQuestionList;
    private int mNextQuestionIndex;

    public QuestionBank(List<Question> questionList) { //permet de changer aleatoirement l'ordre des questions
       mQuestionList = questionList;
       Collections.shuffle(mQuestionList);
    }

    public Question getCurrentQuestion() { //return la question actuel
        return mQuestionList.get(mNextQuestionIndex);
    }

    public Question getNextQuestion() { //return la question suivante
        mNextQuestionIndex++;
        return getCurrentQuestion();
    }
}
