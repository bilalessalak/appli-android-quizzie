package com.example.game.model;

public class User {
    private String mFirstName;
    private int mscore;

    public String getmFirstName() {
        return mFirstName;
    }

    public void setmFirstName(String mFirstName) {
        this.mFirstName = mFirstName;
    }

    public int getMscore() {
        return mscore;
    }

    public void setMscore(int mscore) {
        this.mscore = mscore;
    }
}
