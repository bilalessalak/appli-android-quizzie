package com.example.game.controller;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.game.R;
import com.example.game.model.Question;
import com.example.game.model.QuestionBank;

import java.util.Arrays;

public class GameActivity extends AppCompatActivity implements View.OnClickListener{

    TextView mTextView;
    Button mbtn1;
    Button mbtn2;
    Button mbtn3;
    Button mbtn4;
    QuestionBank mQuestionBank = generateQuestionsBank();
    Question mCurrentQuestion;
    private int mRemainingQuestionCount;
    private int mScore;
    public static final String BUNDLE_EXTRA_SCORE = "BUNDLE_EXTRA_SCORE";
    private boolean mEnableTouchEvents;
    public static final String BUNDLE_STATE_SCORE = "BUNDLE_STATE_SCORE";
    public static final String BUNDLE_STATE_QUESTION = "BUNDLE_STATE_QUESTION";


    @Override
    public boolean dispatchTouchEvent(MotionEvent ev){
        return mEnableTouchEvents && super.dispatchTouchEvent(ev);
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt(BUNDLE_STATE_SCORE, mScore);
        outState.putInt(BUNDLE_STATE_QUESTION, mRemainingQuestionCount);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        mTextView = findViewById(R.id.game_activity_textview_question);
        mbtn1 = findViewById(R.id.game_activity_button_1);
        mbtn2 = findViewById(R.id.game_activity_button_2);
        mbtn3 = findViewById(R.id.game_activity_button_3);
        mbtn4 = findViewById(R.id.game_activity_button_4);

        mbtn1.setOnClickListener(this);
        mbtn2.setOnClickListener(this);
        mbtn3.setOnClickListener(this);
        mbtn4.setOnClickListener(this);

        mCurrentQuestion = mQuestionBank.getCurrentQuestion();
        displayQuestion(mCurrentQuestion);

        mEnableTouchEvents = true ;

        if(savedInstanceState != null){
            mRemainingQuestionCount = savedInstanceState.getInt(BUNDLE_STATE_QUESTION);
            mScore = savedInstanceState.getInt(BUNDLE_STATE_SCORE);
        }
        else{
            mRemainingQuestionCount = 4;
            mScore = 0;
        }
    }

    private void displayQuestion(final Question question){ //affiche la question avec les choix
        mTextView.setText(question.getQuestion());
        mbtn1.setText(question.getmChoiceList().get(0));
        mbtn2.setText(question.getmChoiceList().get(1));
        mbtn3.setText(question.getmChoiceList().get(2));
        mbtn4.setText(question.getmChoiceList().get(3));
    }

    private QuestionBank generateQuestionsBank(){ //instancie des questions
        Question question1 = new Question(
                "Qui est le créateur d'Android ?",
                Arrays.asList(
                        "Andy Rubin",
                        "Steve Wozniak",
                        "Jake Wharton",
                        "Paul Smith"
                ),
                0
        );

        Question question2 = new Question(
                "Combien y a-t-il de départements français ?",
                Arrays.asList(
                        "99",
                        "101",
                        "108",
                        "113"
                ),
                1
        );

        Question question3 = new Question(
                "Quel est l'os humain le plus fracturé ?",
                Arrays.asList(
                        "Tibias",
                        "Crane",
                        "Femur",
                        "Clavicule"
                ),
                3
        );

        Question question4 = new Question(
                "Que signifie USB dans le monde informatique ?",
                Arrays.asList(
                        "Uber States Barbecue",
                        "User Systems Byte",
                        "Universal Serial Bus",
                        "Under Security Bugs"
                ),
                2
        );

        Question question5 = new Question(
                "Quel appareil a été inventé par Henry Mill ?",
                Arrays.asList(
                        "Le réfrigidaire",
                        "La machine à écrire",
                        "La machine à laver",
                        "La télévision"
                ),
                1
        );

        Question question6 = new Question(
                "Quelle héroïne de Disney se voit refuser le droit de rester au bal après minuit?",
                Arrays.asList(
                        "Blanche Neige",
                        "Cedrillon",
                        "Elsa",
                        "Belle"
                ),
                1
        );

        Question question7 = new Question(
                "Quel est le nom du Pilier du Son dans Demon Slayer ?",
                Arrays.asList(
                        "Giyu Tomioka",
                        "Muichiro Tokito",
                        "Tengen Uzui",
                        "Kyojuro Rengoku"
                ),
                2
        );

        Question question8 = new Question(
                "Quel est le héros de la Partie 7 de Jojo Bizarre Adventure ?",
                Arrays.asList(
                        "Giorno Giovanna",
                        "Joesph Joestar",
                        "Rohan Kishibe",
                        "Johnny Joestar"
                ),
                3
        );

        Question question9 = new Question(
                "Selon la légende, comment le pape Adrien IV est-il mort en 1159 ?",
                Arrays.asList(
                        "En avalant une mouche",
                        "En chutant d'un cheval",
                        "en tombant d'un balcon",
                        "En se cognant contre une porte"
                ),
                0
        );

        Question question10 = new Question(
                "Quel état des États-Unis a pour capitale Montgomery ?",
                Arrays.asList(
                        "La Californie",
                        "L'Alabama",
                        "Le Nouveau-Mexique",
                        "L'Ohio"
                ),
                1
        );

        Question question11 = new Question(
                "Quel titan possède Eren Yager dans l'Attaque des Titans ?",
                Arrays.asList(
                        "Le titan Colossal",
                        "Le titan Cuirassé",
                        "Le titan Assaillant",
                        "Le titan Fall 2"
                ),
                2
        );

        Question question12 = new Question(
                "Quel est la carte typique d'un raciste dans Clash Royale ?",
                Arrays.asList(
                        "Le Géant Royal",
                        "Le Méga Chevalier",
                        "Le Mini Pekka",
                        "Le Chauvaucheur de cochon"
                ),
                1
        );

        Question question13 = new Question(
                "Quel animal est un roitelet ?",
                Arrays.asList(
                        "Un poisson",
                        "Un canidé",
                        "Un félin",
                        "Un oiseau"
                ),
                3
        );

        Question question14 = new Question(
                "Quel est le corsaire qui a affronté Roronoa Zoro au début One Piece ? ?",
                Arrays.asList(
                        "Don Quichotte Doflamingo",
                        "Dracule Mihawk",
                        "Crocodile",
                        "Boa Hancock"
                ),
                1
        );

        Question question15 = new Question(
                "Comment se nomme le frère de Thor ?",
                Arrays.asList(
                        "Soki",
                        "Noki",
                        "Loki",
                        "Moqi"
                ),
                2
        );

        Question question16 = new Question(
                "Contrairement à l’imaginaire collectif, laquelle de ces propositions est un fruit  ?",
                Arrays.asList(
                        "Le Haricot vert",
                        "La Carotte",
                        "La Pomme de terre",
                        "L' Aubergine"
                ),
                0
        );


        return new QuestionBank(Arrays.asList(question1, question2, question3, question4, question5, question6, question7, question8, question9, question10, question11, question12, question13, question14, question15, question16)); //crée la liste des questions
    }

    @Override
    public void onClick(View view) {
        int index;

        if (view == mbtn1) {
            index = 0;
        } else if (view == mbtn2) {
            index = 1;
        } else if (view == mbtn3) {
            index = 2;
        } else if (view == mbtn4) {
            index = 3;
        } else {
            throw new IllegalStateException("Unknown clicked view : " + view);
        }

        if (index == mQuestionBank.getCurrentQuestion().getmAnswerIndex()){
            Toast.makeText(this, "Correct !", Toast.LENGTH_SHORT).show();
            mScore++;
        } else {
            Toast.makeText(this, "Incorrect !", Toast.LENGTH_LONG).show();
        }

        mEnableTouchEvents = false;

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mRemainingQuestionCount--;

                if (mRemainingQuestionCount > 0) {
                    mCurrentQuestion = mQuestionBank.getNextQuestion();
                    displayQuestion(mCurrentQuestion);
                } else {
                    endGame();
                }
                mEnableTouchEvents = true;
            }
        }, 2000);


    }

    private void endGame(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("Tu as terminé le Quizz !")
                .setMessage("Voici ton Score: " + mScore)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent();
                        intent.putExtra(BUNDLE_EXTRA_SCORE, mScore);
                        setResult(RESULT_OK, intent);
                        finish();
                    }
                })
                .create()
                .show();
    }

}
