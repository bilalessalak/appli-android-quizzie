package com.example.game.controller;

        import androidx.annotation.Nullable;
        import androidx.appcompat.app.AppCompatActivity;

        import android.content.Intent;
        import android.graphics.Color;
        import android.os.Bundle;
        import android.text.Editable;
        import android.text.TextWatcher;
        import android.view.View;
        import android.widget.CompoundButton;
        import android.widget.EditText;
        import android.widget.Button;
        import android.widget.TextView;
        import android.widget.Switch;

        import com.example.game.R;

public class MainActivity extends AppCompatActivity {
    private EditText edit;
    private Button btn;
    private TextView texte;
    private Switch nintendo;
    private Button gamebtn;
    private static final int GAME_ACTIVITY_REQUEST_CODE = 42;
    private static final String SHARED_PREF_USER_INFO = "SHARED_PREF_USER_INFO";
    private static final String SHARED_PREF_USER_INFO_NAME = "SHARED_PREF_USER_INFO_NAME";
    private static final String SHARED_PREF_USER_INFO_SCORE = "SHARED_PREF_USER_INFO_SCORE";

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (GAME_ACTIVITY_REQUEST_CODE == requestCode && RESULT_OK == resultCode) {
            // Fetch the score from the Intent
            String name = getSharedPreferences(SHARED_PREF_USER_INFO, MODE_PRIVATE).getString(SHARED_PREF_USER_INFO_NAME, null);
            int score = data.getIntExtra(GameActivity.BUNDLE_EXTRA_SCORE, 0);
            getSharedPreferences(SHARED_PREF_USER_INFO, MODE_PRIVATE).edit().putInt(SHARED_PREF_USER_INFO_SCORE, score).apply();
            texte.setText("Bienvenue "+name+" !\n Votre dernier score est: "+score+"\n Vous allez retenter votre chance ?");

        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        edit = findViewById(R.id.edit);
        btn = findViewById(R.id.button);
        btn.setEnabled(false);
        texte = findViewById(R.id.texte);
        nintendo = findViewById(R.id.nintendo);
        nintendo.setChecked(true);
        gamebtn = findViewById(R.id.gamebtn);

        String name = getSharedPreferences(SHARED_PREF_USER_INFO, MODE_PRIVATE).getString(SHARED_PREF_USER_INFO_NAME, null);
        int e_score = getSharedPreferences(SHARED_PREF_USER_INFO, MODE_PRIVATE).getInt(SHARED_PREF_USER_INFO_SCORE, 0);
        texte.setText("Bienvenue "+name+" !");

        edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void afterTextChanged(Editable editable) {
                btn.setEnabled(!editable.toString().isEmpty());
            }
        });

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()){
                    case R.id.button: {
                        edit.clearComposingText();
                        Editable e = edit.getText();
                        texte.setText("Bienvenue "+e);
                        texte.clearComposingText();
                        getSharedPreferences(SHARED_PREF_USER_INFO, MODE_PRIVATE).edit().putString(SHARED_PREF_USER_INFO_NAME, edit.getText().toString()).apply();
                        break;
                    }
                }
            }
        });

        nintendo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b) {
                    texte.setTextColor(Color.RED);
                }
                else {
                    texte.setTextColor(Color.GREEN);
                }

            }
        });

        gamebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent gameActivityIntent = new Intent(MainActivity.this, GameActivity.class);
                startActivityForResult(gameActivityIntent, GAME_ACTIVITY_REQUEST_CODE);
            }
        });
    }
}